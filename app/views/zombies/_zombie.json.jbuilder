json.extract! zombie, :id, :name, :string, :bio, :age, :created_at, :updated_at
json.url zombie_url(zombie, format: :json)
